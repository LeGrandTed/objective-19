/**
 * @defgroup   MAIN main
 *
 * @brief      main file of the project for the project.
 *
 * @author     Luca Faubourg
 * @date       2023
 */

#include "../lib/reset_utils.h"
#include "../lib/log.h"
#include "../lib/libsi7021.h"
#include "../lib/alarm.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <string.h>

#define PIPE1_READ 0
#define PIPE1_WRITE 1
#define PIPE2_READ 2
#define PIPE2_WRITE 3

#define ALERTE_TEMPERATURE 26
#define ALERTE_HUMIDITE 50

// Routine pour la récupération des données
void * si7021Routine(void * arg)
{

    int * fd = (int *)arg;
    struct humidity hum ;
    struct temperature temp;

    while (1) {
    	hum = getHumidity();
        // Envoie l'humidité via la pipe
        if (write(fd[PIPE1_WRITE], &hum, sizeof(struct humidity)) == -1) {
            printf("Error : write humidity");
            writeLog("Error :write humidity");
			writeUART("Error :write humidity");
            exit(EXIT_FAILURE);
        }

        temp = getTemperature();
        // Envoie la temperature via la pipe
        if (write(fd[PIPE2_WRITE], &temp, sizeof(struct temperature)) == -1) {
            printf("Error :write temperature");
            writeLog("Error :write temperature");
			writeUART("Error :write temperature");
            exit(EXIT_FAILURE);
        }
        // Attends une seconde
        sleep(1);
    }

    return NULL;
}

// Routine pour le bouton reset
void * resetRoutine(void * arg)
{
	while(1)
	{
		listenReset();
		printf("RESET\n");
		writeLog("RESET");
		writeUART("RESET");
	}
}

// Affiches les differents typers d'erreur pour le capteur
void displayError(int error) {
    switch (error) {
        case 1:
            printf("Error : Command not sent\n");
            writeLog("Error : Command not sent");
			writeUART("Error : Command not sent");
            break;
        case 2:
            printf("Error : Input/output Error\n");
            writeLog("Error : Input/output Error");
			writeUART("Error : Input/output Error");
            break;
        default:
            printf("Unknown error\n");
            writeLog("Unknown error");
			writeUART("Unknown error");
            break;
    }
}

// Routine pour l'affichage des données
void *displayRoutine(void *arg) {
    int * fd = (int *)arg;
    struct humidity hum;
    struct temperature temp;

    while (1) {
        // Attend la réception de l'humidité via la pipe
        if (read(fd[PIPE1_READ], &hum, sizeof(struct humidity)) == -1) {
            printf("Error : read humidity");
            writeLog("Error : read humidity");
			writeUART("Error : read humidity");
            exit(EXIT_FAILURE);
        }
        if (hum.error == 0)
        {
        	printf("Relative Humidity : %.2f RH \n", hum.humidity);
        	if (hum.humidity > ALERTE_HUMIDITE)
        	{
        		startAlarm();
	            writeLog("startAlarm Humidity");
				writeUART("startAlarm Humidity");
        	}else{
        		stopAlarm();
	            writeLog("stopAlarm Humidity");
				writeUART("stopAlarm Humidity");        		
        	}
        }
        else
        {
        	displayError(hum.error);
        }

        // Attend la réception de la température via la pipe
        if (read(fd[PIPE2_READ], &temp, sizeof(struct temperature)) == -1) {
            printf("Error : read temperature");
            writeLog("Error : read temperature");
			writeUART("Error : read temperature");
            exit(EXIT_FAILURE);
        }
        if (temp.error == 0)
        {
        	printf("Temperature in Celsius : %.2f °C \n", temp.celsius);
        	if (temp.celsius > ALERTE_TEMPERATURE)
        	{
        		startAlarm();
	            writeLog("startAlarm Temperature");
				writeUART("startAlarm Temperature"); 
        	}else{
        		stopAlarm();
	            writeLog("stopAlarm Temperature");
				writeUART("stopAlarm Temperature"); 
        	}
        }
        else
        {
        	displayError(temp.error);
        }

        // Attends une seconde
        sleep(1);
    }

    return NULL;
}


int main(int argc, char const *argv[])
{
	void * res;
	int error;
	pthread_t tid[3];

	char *chipname = "gpiochip0";
	unsigned int resetPin = 17;
	unsigned int alarmPin = 4;
	error = initAlarm(chipname,alarmPin);
	if(error == 1)
	{
		printf("Error : Open alarm chip failed\n");
		writeLog("Error : Open alarm chip failed");
		writeUART("Error : Open alarm chip failed");
		return 0;
	}
	else if (error == 2)
	{
		printf("Error : Get alarm line failed\n");
		writeLog("Error : Get alarm line failed");
		writeUART("Error : Get alarm line failed");
		return 0;
	}
	else if (error == 3)
	{
		printf("Error : Request output alarm failed\n");
		writeLog("Error : Request output alarm failed");
		writeUART("Error : Request output alarm failed");
	}


	error = initReset(chipname,resetPin);
	if(error == 1)
	{
		printf("Error : Open reset chip failed\n");
		writeLog("Error : Open reset chip failed");
		writeUART("Error : Open reset chip failed");
		return 0;
	}
	else if (error == 2)
	{
		printf("Error : Get reset line failed\n");
		writeLog("Error : Get reset line failed");
		writeUART("Error : Get reset line failed");
		return 0;
	}
	else if (error == 3)
	{
		printf("Error : Request event reset failed\n");
		writeLog("Error : Request output reset failed");
		writeUART("Error : Request output reset failed");
	}

  	int fd[4];
	// Crée les deux pipes
    if (pipe(&fd[0]) == -1 || pipe(&fd[2]) == -1) {
        printf("Error : PIPE\n");
        writeLog("Error : PIPE");
		writeUART("Error : PIPE");
        return 0;
    }

	char * busI2C = "/dev/i2c-1";
	if(initSi7021(busI2C))
	{
		printf("Failed to open the bus.\n");
		writeLog("Failed to open the bus.");
		writeUART("Failed to open the bus.");
		return 0;
	}

	error = pthread_create(&(tid[0]), NULL, &si7021Routine,(void*)fd);
	if (error != 0) {
		printf("Error to create si7021Routine\n");
		writeLog("Error to create si7021Routine");
		writeUART("Error to create si7021Routine");
	}

	error = pthread_create(&(tid[1]), NULL, &displayRoutine,(void*)fd);
	if (error != 0) {
		printf("Error to create displayRoutine\n");
		writeLog("Error to create displayRoutine");
		writeUART("Error to create displayRoutine");
	}

	error = pthread_create(&(tid[2]), NULL, &resetRoutine,NULL);
	if (error != 0) {
		printf("Error to create resetRoutine\n");
		writeLog("Error to create resetRoutine");
		writeUART("Error to create resetRoutine");
	}

	for (int i = 0; i < 3; ++i)
	{
		error = pthread_join(tid[i],&res);
		if(error != 0)
		{
			printf("Error to join thread\n");
			writeLog("Error to join thread");
			writeUART("Error to join thread");
		}
	}

	return 0;
}
