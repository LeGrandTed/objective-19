#include "../lib/libsi7021.h"

int file;

int initSi7021(char * busI2C)
{
    file = open(busI2C, O_RDWR);
    if (file < 0) {
        return 1;
    }

    ioctl(file, I2C_SLAVE, 0x40);

    return 0;
}



struct humidity getHumidity()
{
	struct humidity hum = {0,23};
    char config = 0xF5;
    if (write(file, &config, 1) < 1) {
        hum.error = 1;

    } else {
        // Read 2 bytes of humidity data
        // humidity msb, humidity lsb
        char data[2] = {0, 0};
        if (read(file, data, 2) != 2) {
            hum.error = 2;
    
        } else {
            // Convert the data
            hum.humidity = (((float) (data[0] * 256 + data[1]) * 125.0f) / 65536.0f) - 6.f;
            hum.error = 0;
    
        }
    }
    return hum;
}


struct temperature getTemperature()
{
	struct temperature temp = {0,0};

    char config = 0xF3;
    if (write(file, &config, 1) < 1) {
        temp.error = 1;
    } else {
        char data[2] = {0, 0};

        // Read 2 bytes of temperature data
        // temp msb, temp lsb
        if (read(file, data, 2) != 2) {
            temp.error = 2;
        } else {
            // Convert the data
            temp.celsius  = (((float) (data[0] * 256 + data[1]) * 175.72f) / 65536.0f) - 46.85f;
            temp.error = 0;
        }
    }
	return temp;
}


