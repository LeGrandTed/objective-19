#include "../lib/log.h"


void writeUART(const char* message)
{
    FILE* file = fopen("/dev/ttyS0", "a");
	if (file == NULL) {
        printf("Error opening UART.\n");
        return;
    }
    fprintf(file, "%s\n",message);

    fclose(file);
}


void writeLog(const char* message)
{
    time_t now = time(NULL);
    struct tm *t = localtime(&now);

    FILE* file = fopen("./log/ProjectLog.log", "a+");
    if (file == NULL) {
        printf("Error opening log file.\n");
        return;
    }

    fprintf(file, "[%02d:%02d:%02d] %s\n", t->tm_hour, t->tm_min, t->tm_sec, message);

    fclose(file);
}
