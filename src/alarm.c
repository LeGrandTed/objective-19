

#include "../lib/alarm.h"

struct gpiod_line * alarmLine;
struct gpiod_chip * alarmChip;


int initAlarm(char *chipname, unsigned int alarmPin)
{

	alarmChip = gpiod_chip_open_by_name(chipname);
	if(!alarmChip)
	{
		gpiod_chip_close(alarmChip);
		return 1;
	}

	alarmLine = gpiod_chip_get_line(alarmChip,alarmPin);
	if(!alarmLine)
	{
		gpiod_line_release(alarmLine);
		gpiod_chip_close(alarmChip);
		return 2;
	}
	if(gpiod_line_request_output(alarmLine,"alarm",1))
	{
		gpiod_line_release(alarmLine);
		gpiod_chip_close(alarmChip);
		return 3;
	}

	return 0;
}



void startAlarm()
{
	gpiod_line_set_value(alarmLine,0);
}



void stopAlarm()
{
	gpiod_line_set_value(alarmLine,1);
}




void resetAlarm()
{
	gpiod_line_release(alarmLine);
	gpiod_chip_close(alarmChip);
}
