#include "../lib/reset_utils.h"

struct gpiod_line * resetLine;
struct gpiod_chip * resetChip;
struct gpiod_line_event resetEvent;


int initReset(char *chipname, unsigned int resetPin)
{

	resetChip = gpiod_chip_open_by_name(chipname);
	if(!resetChip)
	{
		gpiod_chip_close(resetChip);
		return 1;
	}

	resetLine = gpiod_chip_get_line(resetChip,resetPin);
	if(!resetLine)
	{
		gpiod_line_release(resetLine);
		gpiod_chip_close(resetChip);
		return 2;
	}

	if(gpiod_line_request_falling_edge_events(resetLine,"reset")<0)
	{
		gpiod_line_release(resetLine);
		gpiod_chip_close(resetChip);

		return 3;
	}

	return 0;
}




int listenReset()
{
	while(1)
	{
		gpiod_line_event_wait(resetLine,NULL);
		gpiod_line_event_read(resetLine,&resetEvent);
		return 0;
	}
}



void closeReset()
{
	gpiod_line_release(resetLine);
	gpiod_chip_close(resetChip);
}
