## Projet L3SNIO - Programmation système avec le capteur Adafruit Si7021 et Raspberry Pi
Ce projet consiste à utiliser le capteur Adafruit Si7021 pour détecter la température et l'humidité toutes les secondes. Le système est installé sur un Raspberry Pi, équipé d'un bouton reset et d'un gestionnaire de logs. L'affichage, la récupération des données et la fonction de reset sont développés en programmation partagée.

## Etudiant
Luca Faubourg

## Documentation
Une documentation générée avec Doxygen est disponible dans le dossier "doc", et une version PDF est également disponible à la racine du Git.

## Compilation
Pour compiler le projet, exécutez simplement la commande \`make`.

## Éxécution
Pour exécuter le projet, utilisez la commande \`make run`.


## Connections
|Composant                    | Pin                         |
|-----------------------------|-----------------------------|
|Led                          |  GPIO 4                     |
|Bouton                       |  GPIO 17                    |
|Capteur Adafruit Si7021      |  GPIO 2 (SDA), GPIO 3 (SCL) |
