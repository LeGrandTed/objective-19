SRC_DIR = src
OBJ_DIR = obj
BIN_DIR = bin
CFLAGS = -Wall -Werror
LIBS = -lgpiod -pthread
CC = gcc

all: main

libsi7021.o:
	mkdir -p $(OBJ_DIR)
	$(CC) $(CFLAGS) -c $(SRC_DIR)/libsi7021.c -o $(OBJ_DIR)/libsi7021.o

reset_utils.o:
	mkdir -p $(OBJ_DIR)
	$(CC) $(CFLAGS) -c $(SRC_DIR)/reset_utils.c -o $(OBJ_DIR)/reset_utils.o

alarm.o :
	mkdir -p $(OBJ_DIR)
	$(CC) $(CFLAGS) -c $(SRC_DIR)/alarm.c $(LIBS) -o $(OBJ_DIR)/alarm.o

log.o :
	mkdir -p $(OBJ_DIR)
	$(CC) $(CFLAGS) -c $(SRC_DIR)/log.c -o $(OBJ_DIR)/log.o

main: $(SRC_DIR)/main.c libsi7021.o alarm.o log.o reset_utils.o
	mkdir -p $(BIN_DIR)
	$(CC) $(CFLAGS) $(SRC_DIR)/main.c $(OBJ_DIR)/libsi7021.o $(OBJ_DIR)/alarm.o $(OBJ_DIR)/reset_utils.o $(OBJ_DIR)/log.o $(LIBS) -o $(BIN_DIR)/main

run:
	./$(BIN_DIR)/main

clean:
	rm -rf $(OBJ_DIR)/* $(BIN_DIR)/* $(OBJ_DIR) $(BIN_DIR)
	echo "" > log/ProjectLog.log
