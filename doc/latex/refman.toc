\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble \let \etoc@setlocaltop \@gobble }{}
\contentsline {chapter}{\numberline {1}Module Index}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Modules}{1}{section.1.1}%
\contentsline {chapter}{\numberline {2}Class Index}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Class List}{3}{section.2.1}%
\contentsline {chapter}{\numberline {3}File Index}{5}{chapter.3}%
\contentsline {section}{\numberline {3.1}File List}{5}{section.3.1}%
\contentsline {chapter}{\numberline {4}Module Documentation}{7}{chapter.4}%
\contentsline {section}{\numberline {4.1}main}{7}{section.4.1}%
\contentsline {section}{\numberline {4.2}alarm}{7}{section.4.2}%
\contentsline {section}{\numberline {4.3}libsi7021}{8}{section.4.3}%
\contentsline {section}{\numberline {4.4}log}{8}{section.4.4}%
\contentsline {section}{\numberline {4.5}reset utilities}{8}{section.4.5}%
\contentsline {chapter}{\numberline {5}Class Documentation}{9}{chapter.5}%
\contentsline {section}{\numberline {5.1}humidity Struct Reference}{9}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}Detailed Description}{9}{subsection.5.1.1}%
\contentsline {subsection}{\numberline {5.1.2}Member Data Documentation}{9}{subsection.5.1.2}%
\contentsline {subsubsection}{\numberline {5.1.2.1}error}{9}{subsubsection.5.1.2.1}%
\contentsline {subsubsection}{\numberline {5.1.2.2}humidity}{9}{subsubsection.5.1.2.2}%
\contentsline {section}{\numberline {5.2}temperature Struct Reference}{10}{section.5.2}%
\contentsline {subsection}{\numberline {5.2.1}Detailed Description}{10}{subsection.5.2.1}%
\contentsline {subsection}{\numberline {5.2.2}Member Data Documentation}{10}{subsection.5.2.2}%
\contentsline {subsubsection}{\numberline {5.2.2.1}celsius}{10}{subsubsection.5.2.2.1}%
\contentsline {subsubsection}{\numberline {5.2.2.2}error}{10}{subsubsection.5.2.2.2}%
\contentsline {chapter}{\numberline {6}File Documentation}{11}{chapter.6}%
\contentsline {section}{\numberline {6.1}lib/alarm.h File Reference}{11}{section.6.1}%
\contentsline {subsection}{\numberline {6.1.1}Function Documentation}{12}{subsection.6.1.1}%
\contentsline {subsubsection}{\numberline {6.1.1.1}initAlarm()}{12}{subsubsection.6.1.1.1}%
\contentsline {subsubsection}{\numberline {6.1.1.2}resetAlarm()}{13}{subsubsection.6.1.1.2}%
\contentsline {subsubsection}{\numberline {6.1.1.3}startAlarm()}{13}{subsubsection.6.1.1.3}%
\contentsline {subsubsection}{\numberline {6.1.1.4}stopAlarm()}{13}{subsubsection.6.1.1.4}%
\contentsline {section}{\numberline {6.2}lib/libsi7021.h File Reference}{13}{section.6.2}%
\contentsline {subsection}{\numberline {6.2.1}Function Documentation}{14}{subsection.6.2.1}%
\contentsline {subsubsection}{\numberline {6.2.1.1}getHumidity()}{14}{subsubsection.6.2.1.1}%
\contentsline {subsubsection}{\numberline {6.2.1.2}getTemperature()}{15}{subsubsection.6.2.1.2}%
\contentsline {subsubsection}{\numberline {6.2.1.3}initSi7021()}{15}{subsubsection.6.2.1.3}%
\contentsline {section}{\numberline {6.3}lib/log.h File Reference}{16}{section.6.3}%
\contentsline {subsection}{\numberline {6.3.1}Function Documentation}{17}{subsection.6.3.1}%
\contentsline {subsubsection}{\numberline {6.3.1.1}writeLog()}{17}{subsubsection.6.3.1.1}%
\contentsline {subsubsection}{\numberline {6.3.1.2}writeUART()}{17}{subsubsection.6.3.1.2}%
\contentsline {section}{\numberline {6.4}lib/reset\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}utils.h File Reference}{18}{section.6.4}%
\contentsline {subsection}{\numberline {6.4.1}Function Documentation}{18}{subsection.6.4.1}%
\contentsline {subsubsection}{\numberline {6.4.1.1}closeReset()}{19}{subsubsection.6.4.1.1}%
\contentsline {subsubsection}{\numberline {6.4.1.2}initReset()}{19}{subsubsection.6.4.1.2}%
\contentsline {subsubsection}{\numberline {6.4.1.3}listenReset()}{19}{subsubsection.6.4.1.3}%
\contentsline {section}{\numberline {6.5}src/alarm.c File Reference}{20}{section.6.5}%
\contentsline {subsection}{\numberline {6.5.1}Function Documentation}{21}{subsection.6.5.1}%
\contentsline {subsubsection}{\numberline {6.5.1.1}initAlarm()}{21}{subsubsection.6.5.1.1}%
\contentsline {subsubsection}{\numberline {6.5.1.2}resetAlarm()}{21}{subsubsection.6.5.1.2}%
\contentsline {subsubsection}{\numberline {6.5.1.3}startAlarm()}{22}{subsubsection.6.5.1.3}%
\contentsline {subsubsection}{\numberline {6.5.1.4}stopAlarm()}{22}{subsubsection.6.5.1.4}%
\contentsline {subsection}{\numberline {6.5.2}Variable Documentation}{22}{subsection.6.5.2}%
\contentsline {subsubsection}{\numberline {6.5.2.1}alarmChip}{22}{subsubsection.6.5.2.1}%
\contentsline {subsubsection}{\numberline {6.5.2.2}alarmLine}{22}{subsubsection.6.5.2.2}%
\contentsline {section}{\numberline {6.6}src/libsi7021.c File Reference}{22}{section.6.6}%
\contentsline {subsection}{\numberline {6.6.1}Function Documentation}{23}{subsection.6.6.1}%
\contentsline {subsubsection}{\numberline {6.6.1.1}getHumidity()}{23}{subsubsection.6.6.1.1}%
\contentsline {subsubsection}{\numberline {6.6.1.2}getTemperature()}{24}{subsubsection.6.6.1.2}%
\contentsline {subsubsection}{\numberline {6.6.1.3}initSi7021()}{24}{subsubsection.6.6.1.3}%
\contentsline {subsection}{\numberline {6.6.2}Variable Documentation}{24}{subsection.6.6.2}%
\contentsline {subsubsection}{\numberline {6.6.2.1}file}{25}{subsubsection.6.6.2.1}%
\contentsline {section}{\numberline {6.7}src/log.c File Reference}{25}{section.6.7}%
\contentsline {subsection}{\numberline {6.7.1}Function Documentation}{25}{subsection.6.7.1}%
\contentsline {subsubsection}{\numberline {6.7.1.1}writeLog()}{25}{subsubsection.6.7.1.1}%
\contentsline {subsubsection}{\numberline {6.7.1.2}writeUART()}{26}{subsubsection.6.7.1.2}%
\contentsline {section}{\numberline {6.8}src/main.c File Reference}{26}{section.6.8}%
\contentsline {subsection}{\numberline {6.8.1}Macro Definition Documentation}{27}{subsection.6.8.1}%
\contentsline {subsubsection}{\numberline {6.8.1.1}ALERTE\_HUMIDITE}{27}{subsubsection.6.8.1.1}%
\contentsline {subsubsection}{\numberline {6.8.1.2}ALERTE\_TEMPERATURE}{27}{subsubsection.6.8.1.2}%
\contentsline {subsubsection}{\numberline {6.8.1.3}PIPE1\_READ}{27}{subsubsection.6.8.1.3}%
\contentsline {subsubsection}{\numberline {6.8.1.4}PIPE1\_WRITE}{28}{subsubsection.6.8.1.4}%
\contentsline {subsubsection}{\numberline {6.8.1.5}PIPE2\_READ}{28}{subsubsection.6.8.1.5}%
\contentsline {subsubsection}{\numberline {6.8.1.6}PIPE2\_WRITE}{28}{subsubsection.6.8.1.6}%
\contentsline {subsection}{\numberline {6.8.2}Function Documentation}{28}{subsection.6.8.2}%
\contentsline {subsubsection}{\numberline {6.8.2.1}displayError()}{28}{subsubsection.6.8.2.1}%
\contentsline {subsubsection}{\numberline {6.8.2.2}displayRoutine()}{29}{subsubsection.6.8.2.2}%
\contentsline {subsubsection}{\numberline {6.8.2.3}main()}{29}{subsubsection.6.8.2.3}%
\contentsline {subsubsection}{\numberline {6.8.2.4}resetRoutine()}{31}{subsubsection.6.8.2.4}%
\contentsline {subsubsection}{\numberline {6.8.2.5}si7021Routine()}{31}{subsubsection.6.8.2.5}%
\contentsline {section}{\numberline {6.9}src/reset\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}utils.c File Reference}{32}{section.6.9}%
\contentsline {subsection}{\numberline {6.9.1}Function Documentation}{32}{subsection.6.9.1}%
\contentsline {subsubsection}{\numberline {6.9.1.1}closeReset()}{32}{subsubsection.6.9.1.1}%
\contentsline {subsubsection}{\numberline {6.9.1.2}initReset()}{33}{subsubsection.6.9.1.2}%
\contentsline {subsubsection}{\numberline {6.9.1.3}listenReset()}{33}{subsubsection.6.9.1.3}%
\contentsline {subsection}{\numberline {6.9.2}Variable Documentation}{34}{subsection.6.9.2}%
\contentsline {subsubsection}{\numberline {6.9.2.1}resetChip}{34}{subsubsection.6.9.2.1}%
\contentsline {subsubsection}{\numberline {6.9.2.2}resetEvent}{34}{subsubsection.6.9.2.2}%
\contentsline {subsubsection}{\numberline {6.9.2.3}resetLine}{34}{subsubsection.6.9.2.3}%
\contentsline {chapter}{Index}{35}{section*.30}%
