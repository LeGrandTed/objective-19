var searchData=
[
  ['initalarm_62',['initAlarm',['../alarm_8c.html#ae10643ba3d1a81f34d0a92d8209b3813',1,'initAlarm(char *chipname, unsigned int alarmPin):&#160;alarm.c'],['../alarm_8h.html#ae10643ba3d1a81f34d0a92d8209b3813',1,'initAlarm(char *chipname, unsigned int alarmPin):&#160;alarm.c']]],
  ['initreset_63',['initReset',['../reset__utils_8c.html#af17f612b9afe4584b565928f96552f54',1,'initReset(char *chipname, unsigned int resetPin):&#160;reset_utils.c'],['../reset__utils_8h.html#af17f612b9afe4584b565928f96552f54',1,'initReset(char *chipname, unsigned int resetPin):&#160;reset_utils.c']]],
  ['initsi7021_64',['initSi7021',['../libsi7021_8c.html#ae9181cd542e72bb9c8d01abbdb9cdb01',1,'initSi7021(char *busI2C):&#160;libsi7021.c'],['../libsi7021_8h.html#ae9181cd542e72bb9c8d01abbdb9cdb01',1,'initSi7021(char *busI2C):&#160;libsi7021.c']]]
];
