/**
 * @defgroup   LOG log
 *
 * @brief      This file implements log.
 *
 * @author     Luca Faubourg
 * @date       2023
 */
#ifndef log_H_
#define log_H_

#include <stdio.h>
#include <time.h>



/**
 * @brief      Writes into uart.
 *
 * @param[in]  message  The message to put into the uart
 */
void writeUART(const char* message);


/**
 * @brief      Writes into the log file. (../log/PorjectLog.log)
 *
 * @param[in]  message  The message for the log
 */
void writeLog(const char* message);


#endif /* log_H_ */