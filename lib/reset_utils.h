/**
 * @defgroup   RESET_UTILS reset utilities
 *
 * @brief      This file implements reset utilities.
 *
 * @author     Luca Faubourg
 * @date       2023
 */
#ifndef reset_utils_H_
#define reset_utils_H_

#include <stdio.h>
#include <unistd.h>
#include <gpiod.h>

/**
 * @brief      Initializes the alarm.
 *
 * @param[in]  chipname  The chipname of the reset device
 * @param[in]  alarmPin  The reset pin to listen (Ex : Button)
 *
 * @return     error
 */
int initReset(char *chipname, unsigned int resetPin);


/**
 * @brief      Wait for a falling edge
 *
 * @return     return 0 when falling edge detected
 */
int listenReset();



/**
 * @brief      Closes the reset chip and line.
 */
void closeReset();


#endif /* reset_utils_H_ */
