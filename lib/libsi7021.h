/**
 * @defgroup   LIBSI7021 libsi7021
 *
 * @brief      This file implements libsi7021.
 *
 * @author     Luca Faubourg
 * @date       2023
 * @note       Credits https://github.com/ControlEverythingCommunity/SI7021/blob/master/C/SI7021.c \n Slightly fixed by C. Monière. \n Rearrange into lirary by L.Faubourg.
 */

#ifndef libsi7021_H_
#define libsi7021_H_

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>




/**
 * @brief      structure pour les données d'humidité du capteur
 */
struct humidity
{
	int error;
	float humidity;	
};



/**
 * @brief   structure pour les données de temperature du capteur   
 */
struct temperature
{
	int error;
	float celsius;
};


/**
 * @brief      Initializes the si7021.
 *
 * @param      busI2C  The bus I2C
 *
 * @return     return error status 0 = no error, 1 = error
 */
int initSi7021(char * busI2C);


/**
 * @brief      Gets the humidity data.
 *
 * @return     The struct humidity with error and relative humidity.
 */
struct humidity getHumidity();




/**
 * @brief      Gets the temperature data.
 *
 * @return     The sruct temperature with error and temp in celsius.
 */
struct temperature getTemperature();

#endif /* libsi7021_H_ */
