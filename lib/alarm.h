/**
 * @defgroup   ALARM alarm
 *
 * @brief      This file implements alarm.
 *
 * @author     Luca Faubourg
 * @date       2023
 */

#ifndef alarm_H_
#define alarm_H_

#include <gpiod.h>
#include <unistd.h>

/**
 * @brief      Initializes the alarm.
 *
 * @param[in]  chipname  The chipname of the alarm device
 * @param[in]  alarmPin  The alarm pin (Ex : Led)
 *
 * @return     error
 */
int initAlarm(char *chipname, unsigned int alarmPin);

/**
 * @brief      put the alarm Pin at 0
 */
void startAlarm();

/**
 * @brief      put the alarm Pin at 1
 */
void stopAlarm();

/**
 * @brief      close and release the chip and line
 */
void resetAlarm();

#endif /* alarm_H_ */
